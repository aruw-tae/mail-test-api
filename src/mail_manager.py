import configparser
import email
import imaplib

config = configparser.ConfigParser()
config.read("config/general.ini")

def read_mails_from(from_mail):
    emails_list = []
    mail = imaplib.IMAP4_SSL(config['MAIL']['server'],port=config['MAIL']['port_imap'])

    mail.login(config['MAIL']['user'],config['MAIL']['password'])

    mail.select('inbox')

    status, data = mail.search(None,f'(FROM "{from_mail}")')
    mail_ids = []

    for block in data:
        mail_ids += block.split()

    for i in mail_ids:
        status, data = mail.fetch(i, '(RFC822)')
        for response_part in data:
            if isinstance(response_part, tuple):
                message = email.message_from_bytes(response_part[1])
                mail_from = message['from']
                mail_subject = message['subject']
                if message.is_multipart():
                    mail_content = ''
                    for part in message.get_payload():
                        if part.get_content_type() == 'text/plain':
                            mail_content += part.get_payload()
                else:
                    mail_content = message.get_payload()
                emails_list.append({
                    'from':  mail_from,
                    'subject': mail_subject,
                    'content': mail_content
                })
    mail.shutdown()
    return { 'messages':emails_list}