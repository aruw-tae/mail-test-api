from flask import Flask,jsonify
import mail_manager

app = Flask(__name__)


@app.route('/inbox/messages/<string:from_mail>')
def list_messages(from_mail):
    return jsonify(mail_manager.read_mails_from(from_mail))


if __name__ == "__main__":
    app.run(port=3030)